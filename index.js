const create_tourist_place_ele = document.getElementById("create-tourist_place");
const edit_tourist_place_ele = document.getElementById("edit-tourist_place");
const tourist_page_list_link = document.getElementById("tourist_page_list_link");
const create_new_tourist_page_link = document.getElementById("create_new_tourist_page_link");
const input_fields_info = document.querySelectorAll(".form-item input[data-info]");
const picture_field = document.getElementById("id_picture");
const tourist_places_ele = document.getElementById("tourist_places");
const form_submit_btn = document.querySelector(".from-btn-submit");

const current_page_config = {
    updating_mode: false,
    tourist_index: null,
    is_edit_tourist_place_hidden: true,
    set_form_btn_text() {
        form_submit_btn.innerText = this.updating_mode ? "Update" : "Submit";
    },
};
// make  tourist place list/edit hidden by default
edit_tourist_place_ele.hidden = current_page_config.is_edit_tourist_place_hidden;

const defaultTouristPlace = {
    id: 0,
    name: "",
    address: "",
    rating: null,
    picture: "",
    picture_name: "",
};
let touristPlace = { ...defaultTouristPlace };

const touristPlaceList = [];

//assign page handler
tourist_page_list_link.onclick = pageHandler;
create_new_tourist_page_link.onclick = pageHandler;

// add event listener to all the input field 
populate_form(true);

function populate_form(init_state = false) {
    for (let i = 0; i < input_fields_info.length; i++) {
        const element = input_fields_info[i];
        init_state ? element.addEventListener("change", inputHandler) : null;
        element.value = touristPlace[element.name];
    }
    init_state
        ? picture_field.addEventListener("change", pictureInputHandler)
        : null;
    current_page_config.updating_mode ? (picture_field.required = false) : null;
}

function pageHandler(event=null) {
    if (event) {
        event.preventDefault();
    }
    current_page_config.is_edit_tourist_place_hidden =
        !current_page_config.is_edit_tourist_place_hidden;

    edit_tourist_place_ele.hidden = current_page_config.is_edit_tourist_place_hidden;

    create_tourist_place_ele.hidden =
        !current_page_config.is_edit_tourist_place_hidden;

    current_page_config.set_form_btn_text();
}

function inputHandler(event) {
    touristPlace[event.target.name] = event.target.value;
}

function pictureInputHandler(event) {
    // crate a new file reader instance
    const reader = new FileReader();

    // passing an input file to be read as
    reader.readAsBinaryString(picture_field.files[0]);

    // after reader load/read the file
    reader.onload = () => {
        // result of the file reader store in a object property
        touristPlace["picture"] = reader.result;
        touristPlace["picture_name"] = picture_field.files[0].name;
    };
}
function resetForm() {
    touristPlace = { ...defaultTouristPlace };
    for (let i = 0; i < input_fields_info.length; i++) {
        //resting input field
        input_fields_info[i].value = null;
        input_fields_info[i].classList.remove('invalid');
    }
    picture_field.required = true;
    picture_field.files = null;
    picture_field.value = "";

    // rest the page config
    current_page_config.updating_mode = false;
    current_page_config.tourist_index = null;
    current_page_config.set_form_btn_text();
}

function createTouristAction(event) {
    event.preventDefault();

    //simple validation 
    for (let i = 0; i < input_fields_info.length; i++) {
        const element = input_fields_info[i];
            if(element.type=="number"){
                if(element.value<0 || element.value>6){
                    alert(`[${element.name}] is an invalid field`);
                    element.classList.toggle("invalid");
                    return;
                }
            }else if(element.type=="text"){
                const value=element.value.trim();
                if(value.length<8 || value.length>20){
                    alert(`[${element.name}] field contain more then 7 character and less then 20 character`);
                    element.classList.toggle("invalid");
                    return;
                }
            }
        

    }
    // if current page in updating mode
    // then update the tourist place object
    // and rest the  form
    if (current_page_config.updating_mode && current_page_config.tourist_index) {
        touristPlaceList[current_page_config.tourist_index] = { ...touristPlace };
    } else {
        //check if there any previously added tourist place
        if (touristPlaceList.length == 0) {
            touristPlace.id = 1;
        } else {
            // if exist such last element id will be increment to created new id
            touristPlace.id = touristPlaceList[touristPlaceList.length - 1].id + 1;
        }
        
        touristPlaceList.push({ ...touristPlace });
    }
    resetForm();
    realoadTouristPlaceListUI();
    pageHandler();
}

function createTouristPageItem(touristPlace, index) {
    return `
        <div class="grid-row" data-touristIndex="${index}">
            <div class="grid-item item-name">${touristPlace.name}</div>
            <div class="grid-item item-address">${touristPlace.address}</div>

            <div class="grid-item item-rating">${touristPlace.rating}</div>

            <div class="grid-item item-picture">
                <img
                    src="data:image/jpeg;base64,${btoa(touristPlace.picture)}"
                    alt="${touristPlace.picture_name}"
                    srcset=""
                />
            </div>

            <div class="grid-item item-action">
                <div class="action-btns">
                    <button onclick="deleteTouristPlace(this)" class="action-btn btn-delete" type="submit">
                    Delete
                    </button>
                    <button onclick="updateTouristPlace(this)" class="action-btn btn-update" type="submit">
                    Update
                    </button>
                </div>
            </div>
        </div>
    `;
}

function deleteTouristPlace(element) {

    if (!confirm('Are you sure you want to delete this ?')) return;

    const parent = element.closest(".grid-row");
    const index = parent.dataset.touristindex;
    touristPlaceList.splice(index, 1);
    parent.remove();
    realoadTouristPlaceListUI();
}

function updateTouristPlace(element) {
    current_page_config.updating_mode = true;
    const parent = element.closest(".grid-row");

    if (parent && parent.dataset.touristindex) {
        const index = parent.dataset.touristindex;
        current_page_config["tourist_index"] = index;
        touristPlace = { ...touristPlaceList[index] };
        populate_form();
        pageHandler(); // back to create tourist page
    }
}

// render the ui with default touristPlace or with given place_list
function realoadTouristPlaceListUI(place_list = null) {
    tourist_places_ele.innerHTML = "";

    const list = place_list ? place_list : touristPlaceList;

    for (let i = 0; i < list.length; i++) {
        tourist_places_ele.innerHTML += createTouristPageItem( list[i], i);
    }
}

function search_tourist_place(event) {
    // console.log(event.target.value);

    const search_result = touristPlaceList.filter((place_obj, index) => {
        return place_obj.name.includes(event.target.value);
    });

    realoadTouristPlaceListUI(search_result);
}

function sort_tourist_place(event) {
    if (event.target.value == "ASC" || event.target.value == "DESC") {
        touristPlaceList.sort((place_obj_a, place_obj_b) => {

            if (event.target.value == "ASC") return place_obj_a.rating - place_obj_b.rating;

            if (event.target.value == "DESC") return place_obj_b.rating - place_obj_a.rating;

            return 0;
        });
    } else {
        touristPlaceList.sort((place_obj_a, place_obj_b) => {
            return place_obj_a.id - place_obj_b.id;
        });
    }

    realoadTouristPlaceListUI();
}
